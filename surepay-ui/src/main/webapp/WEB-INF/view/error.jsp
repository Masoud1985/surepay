<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div>
    <spring:message code="err.general.error"/>
</div>
<br>
<div>
    <a href="http://localhost:8080/surepay">
        <input type="button" value="SurePay"/>
    </a>
</div>
</body>
</html>
