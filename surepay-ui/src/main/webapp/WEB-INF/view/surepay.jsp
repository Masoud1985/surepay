<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SurePay</title>
    <style>
        .transaction tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .transaction tr:hover {
            background-color: #ddd;
        }

        .transaction {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .transaction td, .transaction th {
            border: 1px solid #ddd;
            padding: 8px;
        }
    </style>
</head>
<body>
<a href="http://localhost:8080/locale?language=nl">
    <div style="display: inline-block">
    <span style="display: block">
        <img src="<c:url value="/img/nl.png"/>"/>
    </span>
        <span style="font-size: xx-small">
        NL
    </span>
    </div>
</a>
<a href="http://localhost:8080/locale?language=en">
    <div style="display: inline-block; margin-left: 10px">
    <span style="display: block">
        <img src="<c:url value="/img/us.png"/>"/>
    </span>
        <span style="font-size: xx-small">
        EN
    </span>
    </div>
</a>
</br>
<c:if test="${not empty fileUploadMessage}">
    <div style="background-color: lightpink; margin: 0 20px 0 20px; border-radius: 10px; padding: 10px">
        <ul>
            <c:forEach items="${fileUploadMessage}" var="errMesage">
                <li><c:out value="${errMesage}"/></li>
            </c:forEach>
        </ul>
    </div>
</c:if>
<h2>
    <%--    SurePay transaction validation--%>
    <spring:message code="lbl.surepay.transaction.validation"/>
</h2>
<form:form method="POST" action="${pageContext.request.contextPath}/upload" enctype="multipart/form-data">
    <form:label path="file"><spring:message code="lbl.select.file"/></form:label>
    <input type="file" name="file"/>
    <button type="submit"><spring:message code="lbl.button.send"/></button>
</form:form>
<c:if test="${responseData != null}">
    <c:if test="${responseData.DUPLICATION != null}">
        <div>
    <span>
        <h3>
      <spring:message code="lbl.duplicated.transactions"/>:
        </h3>
    </span>
        </div>
        <table class="transaction">
            <tr>
                <th>
                    <spring:message code="lbl.row"/>
                </th>
                <th>
                    <spring:message code="lbl.reference"/>
                </th>
                <th>
                    <spring:message code="lbl.account.number"/>
                </th>
                <th>
                    <spring:message code="lbl.description"/>
                </th>
                <th>
                    <spring:message code="lbl.start.balance"/>
                </th>
                <th>
                    <spring:message code="lbl.mutation"/>
                </th>
                <th>
                    <spring:message code="lbl.end.balance"/>
                </th>
            </tr>
            <c:forEach var="transaction" items="${responseData.DUPLICATION}" varStatus="counter">
                <tr>
                    <td>
                            ${counter.index + 1}
                    </td>
                    <td>
                            ${transaction.reference}
                    </td>
                    <td>
                            ${transaction.accountNumber}
                    </td>
                    <td>
                            ${transaction.description}
                    </td>
                    <td>
                            ${transaction.startBalance}
                    </td>
                    <td>
                            ${transaction.mutation}
                    </td>
                    <td>
                            ${transaction.endBalance}
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${responseData.INVALID_TRANSACTION != null}">
        <div>
    <span>
        <h3>
            <spring:message code="lbl.invalid.transactions"/>
        </h3>
    </span>
        </div>
        <table class="transaction">
            <tr>
                <th>
                    <spring:message code="lbl.row"/>
                </th>
                <th>
                    <spring:message code="lbl.reference"/>
                </th>
                <th>
                    <spring:message code="lbl.account.number"/>
                </th>
                <th>
                    <spring:message code="lbl.description"/>
                </th>
                <th>
                    <spring:message code="lbl.start.balance"/>
                </th>
                <th>
                    <spring:message code="lbl.mutation"/>
                </th>
                <th>
                    <spring:message code="lbl.end.balance"/>
                </th>
            </tr>
            <c:forEach var="transaction" items="${responseData.INVALID_TRANSACTION}" varStatus="counter">
                <tr>
                    <td>
                            ${counter.index + 1}
                    </td>
                    <td>
                            ${transaction.reference}
                    </td>
                    <td>
                            ${transaction.accountNumber}
                    </td>
                    <td>
                            ${transaction.description}
                    </td>
                    <td>
                            ${transaction.startBalance}
                    </td>
                    <td>
                            ${transaction.mutation}
                    </td>
                    <td>
                            ${transaction.endBalance}
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</c:if>
</body>
</html>
