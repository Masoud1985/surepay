package nl.surepay.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SurePayConfiguration {

    @GetMapping("/locale")
    public String locale() {
        return "redirect:/surepay";
    }

}
