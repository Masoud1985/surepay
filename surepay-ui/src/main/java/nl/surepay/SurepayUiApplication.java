package nl.surepay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurepayUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SurepayUiApplication.class, args);
    }

}
