package nl.surepay.controller;

import com.fasterxml.jackson.databind.json.JsonMapper;
import nl.surepay.commmon.ApiResponse;
import okhttp3.*;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Controller
public class FileController {
    private final String VALIDATE_FILE_URL = "http://localhost:8080/process-file/validateUploadedFile";
    private final String TEMP_FILE = "/tmp_folder/targetFile_ui.";
    Logger logger = LoggerFactory.getLogger(FileController.class);

    @GetMapping("/surepay")
    public String main(Model model) {
        model.addAttribute("command", new File("transaction"));
        return "surepay";
    }

    @GetMapping("/")
    public String home() {
        return "redirect:/surepay";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model, RedirectAttributes attributes) {
        model.addAttribute("command", new File("transaction"));
        if (file.isEmpty()) {
            attributes.addFlashAttribute("fileUploadMessage", Arrays.asList("Please select a file to upload"));
            return "redirect:/surepay";
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        File physicalFile = new File(System.getProperty("user.dir") + TEMP_FILE + FilenameUtils.getExtension(fileName));
        try {
            new File(physicalFile.getParent()).mkdirs();
            if (physicalFile.exists()) {
                physicalFile.delete();
            }
            physicalFile.createNewFile();
            file.transferTo(physicalFile);
        } catch (IOException e) {
            logger.debug("There is a problem with working with file, either creating a new one or writing data to " + e.getMessage());
        }
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", "file." + FilenameUtils.getExtension(fileName),
                        RequestBody.create(physicalFile, MediaType.parse("application/octet-stream")))
                .build();
        Request request = new Request.Builder()
                .url(VALIDATE_FILE_URL)
                .post(requestBody)
                .build();
        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        Response response = null;
        ApiResponse apiResponse = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            logger.debug("An error has occurred in calling http://localhost:8080/process-file/validateUploadedFile Api " + e.getMessage());
        }
        if (response.code() != HttpStatus.OK.value()) {
            attributes.addFlashAttribute("fileUploadMessage", Arrays.asList(response.message()));
            return "redirect:/surepay";
        }
        JsonMapper jsonMapper = new JsonMapper();
        try {
            apiResponse = jsonMapper.readValue(response.body().byteStream(), ApiResponse.class);
            if (physicalFile.exists()) {
                physicalFile.delete();
            }
        } catch (IOException e) {
            logger.debug("An error has occurred in parsing the response from http://localhost:8080/process-file/validateUploadedFile Api " + e.getMessage());
        }
        if (apiResponse != null && apiResponse.getSuccess()) {
            model.addAttribute("responseData", apiResponse.getData());
        } else if (apiResponse != null && !apiResponse.getSuccess()) {
            model.addAttribute("fileUploadMessage", apiResponse.getMessage());
        }
        return "surepay";
    }
}
