
package nl.surepay.exception;

import nl.surepay.commmon.ApiResponse;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.List;

@RestControllerAdvice
public class ExceptionInterceptor {

    private MessageSource messageSource;

    public ExceptionInterceptor(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RuntimeException.class)
    public ApiResponse handleUserBusinessExceptions(RuntimeException ex) {
        List<String> message = Arrays.asList(messageSource.getMessage("system.general.error", null, LocaleContextHolder.getLocale()));
        return ApiResponse.of(false, message, null);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UnknownFileException.class)
    public ApiResponse handleUnknownFileException(UnknownFileException ex) {
        List<String> message = Arrays.asList(messageSource.getMessage("error.unknown.file.type", null, LocaleContextHolder.getLocale()));
        return ApiResponse.of(false, message, null);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(JsonDataFormatException.class)
    public ApiResponse handleJsonDataFormatException(JsonDataFormatException ex) {
        List<String> message = Arrays.asList(messageSource.getMessage("error.json.data.format.is.invalid", null, LocaleContextHolder.getLocale()));
        return ApiResponse.of(false, message, null);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CsvDataFormatException.class)
    public ApiResponse handleCsvDataFormatException(CsvDataFormatException ex) {
        List<String> message = Arrays.asList(messageSource.getMessage("error.csv.data.format.is.invalid", null, LocaleContextHolder.getLocale()));
        return ApiResponse.of(false, message, null);
    }
}
