package nl.surepay.enums;


public enum FileType {

    JSON_FILE("json") {
        @Override
        public String getFileExtension() {
            return "json";
        }
    }, CSV_FILE("csv") {
        @Override
        public String getFileExtension() {
            return "csv";
        }
    }, UNKNOWN(null);

    private String value;

    FileType(String fileType) {
        this.value = fileType;
    }

    public static FileType fromString(String value) {
        for (FileType fileType : FileType.values()) {
            if (fileType.value.equalsIgnoreCase(value)) {
                return fileType;
            }
        }
        return UNKNOWN;
    }

    public String getFileExtension() {
        return "unknown";
    }

}
