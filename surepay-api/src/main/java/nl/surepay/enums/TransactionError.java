package nl.surepay.enums;

public enum TransactionError {
    DUPLICATION, INVALID_TRANSACTION;
}
