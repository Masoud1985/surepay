package nl.surepay.service;

import com.opencsv.bean.CsvToBeanBuilder;
import nl.surepay.commmon.ServiceResult;
import nl.surepay.enums.FileType;
import nl.surepay.enums.TransactionError;
import nl.surepay.exception.CsvDataFormatException;
import nl.surepay.exception.UnknownFileException;
import nl.surepay.model.SurePayTransaction;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ValidateCsvData implements ValidateDataService {

    @Override
    public ServiceResult validateData() {
        File file = selectFile("Select file");
        Map prepareTransactionFileData = prepareTransactionFileData(file);
        if (prepareTransactionFileData == null) {
            throw new CsvDataFormatException();
        }
        Map<TransactionError, List<SurePayTransaction>> errorListMap = parseTransactions(prepareTransactionFileData);
        if (errorListMap == null || (errorListMap != null && errorListMap.size() == 0)) {
            return ServiceResult.of(false, Arrays.asList("There is no wrong transaction"), null);
        }
        return ServiceResult.of(true, null, errorListMap);
    }

    @Override
    public ServiceResult validateData(File transactionFile) {
        Map prepareTransactionFileData = prepareTransactionFileData(transactionFile);
        if (prepareTransactionFileData == null) {
            throw new CsvDataFormatException();
        }
        Map<TransactionError, List<SurePayTransaction>> errorListMap = parseTransactions(prepareTransactionFileData);
        if (errorListMap == null || (errorListMap != null && errorListMap.size() == 0)) {
            return ServiceResult.of(false, null, null);
        }
        return ServiceResult.of(true, null, errorListMap);
    }

    private Map<Long, List<SurePayTransaction>> prepareTransactionFileData(File file) {
        if (file != null) {
            if (!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase(getFileType().getFileExtension())) {
                throw new UnknownFileException();
            }
            List<SurePayTransaction> surePayTransactions = null;
            try {
                surePayTransactions = new CsvToBeanBuilder(new FileReader(file.getPath()))
                        .withType(SurePayTransaction.class).withSkipLines(1)
                        .build()
                        .parse();
            } catch (Exception e) {
                throw new CsvDataFormatException();
            }
            Map<Long, List<SurePayTransaction>> result = surePayTransactions.stream().collect(Collectors.groupingBy(SurePayTransaction::getReference));
            return result;
        }
        return null;
    }


    @Override
    public FileType getFileType() {
        return FileType.CSV_FILE;
    }

}
