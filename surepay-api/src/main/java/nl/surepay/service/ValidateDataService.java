package nl.surepay.service;

import nl.surepay.commmon.ServiceResult;
import nl.surepay.enums.FileType;
import nl.surepay.enums.TransactionError;
import nl.surepay.model.SurePayTransaction;

import javax.swing.*;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public interface ValidateDataService {

    public ServiceResult validateData();

    public ServiceResult validateData(File transactionFile);

    public FileType getFileType();

    public default File selectFile(String message) {
        File file = null;
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int result = chooser.showDialog(chooser, message);
        if (result == APPROVE_OPTION) {
            file = chooser.getSelectedFile();
        }
        return file;
    }

    public default Map parseTransactions(Map<Long, List<SurePayTransaction>> result) {
        Map<TransactionError, List<SurePayTransaction>> errorListMap = new HashMap<>();
        for (Object reference : result.keySet()) {
            if (result.get(reference).size() > 1) {
                if (errorListMap.get(TransactionError.DUPLICATION) != null) {
                    errorListMap.get(TransactionError.DUPLICATION).addAll(result.get(reference));
                } else {
                    errorListMap.put(TransactionError.DUPLICATION, result.get(reference));
                }
                continue;
            }
            if (result.get(reference).get(0).getStartBalance().add(result.get(reference).get(0).getMutation()).compareTo(result.get(reference).get(0).getEndBalance()) != 0) {
                if (errorListMap.get(TransactionError.INVALID_TRANSACTION) != null) {
                    errorListMap.get(TransactionError.INVALID_TRANSACTION).addAll(result.get(reference));
                } else {
                    errorListMap.put(TransactionError.INVALID_TRANSACTION, result.get(reference));
                }
            }
        }
        return errorListMap;
    }


}
