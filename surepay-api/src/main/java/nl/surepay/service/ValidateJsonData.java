package nl.surepay.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.surepay.commmon.ServiceResult;
import nl.surepay.enums.FileType;
import nl.surepay.enums.TransactionError;
import nl.surepay.exception.JsonDataFormatException;
import nl.surepay.exception.UnknownFileException;
import nl.surepay.model.SurePayTransaction;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ValidateJsonData implements ValidateDataService {

    @Override
    public ServiceResult validateData() {
        File file = selectFile("Select file");
        Map<Long, List<SurePayTransaction>> prepareTransactionFileData = prepareTransactionFileData(file);
        if (prepareTransactionFileData == null) {
            throw new JsonDataFormatException();
        }
        Map<TransactionError, List<SurePayTransaction>> errorListMap = parseTransactions(prepareTransactionFileData);
        if (errorListMap == null || (errorListMap != null && errorListMap.size() == 0)) {
            return ServiceResult.of(false, Arrays.asList("There is no wrong transaction"), null);
        }
        return ServiceResult.of(true, null, errorListMap);
    }

    @Override
    public ServiceResult validateData(File transactionFile) {
        Map<Long, List<SurePayTransaction>> prepareTransactionFileData = prepareTransactionFileData(transactionFile);
        if (prepareTransactionFileData == null) {
            throw new JsonDataFormatException();
        }
        Map<TransactionError, List<SurePayTransaction>> errorListMap = parseTransactions(prepareTransactionFileData);
        if (errorListMap == null || (errorListMap != null && errorListMap.size() == 0)) {
            return ServiceResult.of(false, Arrays.asList("There is no wrong transaction"), null);
        }
        return ServiceResult.of(true, null, errorListMap);
    }

    private Map<Long, List<SurePayTransaction>> prepareTransactionFileData(File file) {
        if (file != null) {
            if (!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase(getFileType().getFileExtension())) {
                throw new UnknownFileException();
            }
            ObjectMapper objectMapper = new ObjectMapper();
            SurePayTransaction[] surePayTransaction = null;
            try {
                surePayTransaction = objectMapper.readValue(file, SurePayTransaction[].class);
            } catch (IOException e) {
                throw new JsonDataFormatException();
            }
            Map<Long, List<SurePayTransaction>> result = Arrays.stream(surePayTransaction).collect(Collectors.groupingBy(SurePayTransaction::getReference));
            return result;
        }
        return null;
    }

    @Override
    public FileType getFileType() {
        return FileType.JSON_FILE;
    }
}
