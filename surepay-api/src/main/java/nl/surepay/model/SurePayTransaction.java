package nl.surepay.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SurePayTransaction {

    @CsvBindByPosition(position = 0)
    private long reference;
    @CsvBindByPosition(position = 1)
    private String accountNumber;
    @CsvBindByPosition(position = 2)
    private String description;
    @CsvBindByPosition(position = 3)
    private BigDecimal startBalance;
    @CsvBindByPosition(position = 4)
    private BigDecimal mutation;
    @CsvBindByPosition(position = 5)
    private BigDecimal endBalance;

}
