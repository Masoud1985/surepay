package nl.surepay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurepayApiApplication {

    private static final String HEADLESS = "java.awt.headless";

    public static void main(String[] args) {
        System.setProperty(HEADLESS, "false");
        SpringApplication.run(SurepayApiApplication.class, args);
    }

}
