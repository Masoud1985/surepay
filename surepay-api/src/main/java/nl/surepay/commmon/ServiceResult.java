package nl.surepay.commmon;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ServiceResult implements Serializable {

    private boolean valid;
    private List<String> messages;
    private Object data;

    public ServiceResult(boolean valid, List<String> messages) {
        this.valid = valid;
        this.messages = messages;
    }

    public ServiceResult(boolean valid, List<String> messages, Object data) {
        this.valid = valid;
        this.messages = messages;
        this.data = data;
    }

    public static ServiceResult of(boolean success, List<String> messages) {
        return new ServiceResult(success, messages);
    }


    public static ServiceResult of(boolean success, List<String> messages, Object data) {
        return new ServiceResult(success, messages, data);
    }

}
