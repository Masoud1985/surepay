package nl.surepay.commmon;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class ApiResponse implements Serializable {

    private Boolean success;
    private List<String> message;
    private Object data;

    public ApiResponse(Boolean success, List<String> message) {
        this.success = success;
        this.message = message;
    }

    public ApiResponse(Boolean success, List<String> message, Object data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public ApiResponse(Object data) {
        this.success = true;
        this.data = data;
    }

    public static ApiResponse of(Boolean success, List<String> message) {
        return new ApiResponse(success, message);
    }

    public static ApiResponse of(Object data) {
        return new ApiResponse(data);
    }

    public static ApiResponse of(Boolean success, List<String> message, Object data) {
        return new ApiResponse(success, message, data);
    }

}
