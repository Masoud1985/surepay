package nl.surepay.api;

import nl.surepay.commmon.ApiResponse;
import nl.surepay.enums.FileType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public interface FileProcessor {

    @GetMapping("/validateFile")
    public ApiResponse processFile(@RequestParam(name = "fileType", required = true) FileType fileType);

    @PostMapping(value = "/validateUploadedFile")
    public ApiResponse processUploadedFile(@RequestBody(required = true) MultipartFile file);
}
