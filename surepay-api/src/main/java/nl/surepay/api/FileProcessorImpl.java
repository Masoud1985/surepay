package nl.surepay.api;

import nl.surepay.commmon.ApiResponse;
import nl.surepay.commmon.ServiceResult;
import nl.surepay.enums.FileType;
import nl.surepay.service.ValidateDataService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/process-file")
public class FileProcessorImpl implements FileProcessor {
    private final Map<FileType, ValidateDataService> validateDataServiceMap;
    private final String TEMP_FILE = "/tmp_folder/targetFile_api.";
    Logger logger = LoggerFactory.getLogger(FileProcessorImpl.class);

    public FileProcessorImpl(List<ValidateDataService> validateDataServiceList) {
        this.validateDataServiceMap = validateDataServiceList.stream().collect(Collectors.toMap(ValidateDataService::getFileType, Function.identity()));
    }

    @Override
    public ApiResponse processFile(FileType fileType) {
        ServiceResult serviceResult = validateDataServiceMap.get(fileType).validateData();
        if (serviceResult.isValid()) {
            return ApiResponse.of(true, null, serviceResult.getData());
        }
        return ApiResponse.of(false, serviceResult.getMessages(), null);
    }

    @Override
    public ApiResponse processUploadedFile(MultipartFile file) {
        File physicalFile = new File(System.getProperty("user.dir") + TEMP_FILE + FilenameUtils.getExtension(file.getOriginalFilename()));
        try {
            new File(physicalFile.getParent()).mkdirs();
            if (physicalFile.exists()) {
                physicalFile.delete();
            }
            physicalFile.createNewFile();
            file.transferTo(physicalFile);
        } catch (IOException e) {
            logger.debug("There is a problem with working with file, either creating a new one or writing data to " + e.getMessage());
        }
        ServiceResult serviceResult = validateDataServiceMap.get(FileType.fromString(FilenameUtils.getExtension(file.getOriginalFilename()))).validateData(physicalFile);
        if (physicalFile.exists()) {
            System.gc();
            physicalFile.delete();
        }
        if (serviceResult.isValid()) {
            return ApiResponse.of(true, null, serviceResult.getData());
        }
        return ApiResponse.of(false, serviceResult.getMessages(), null);
    }
}
