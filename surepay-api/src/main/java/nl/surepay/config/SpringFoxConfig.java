package nl.surepay.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("nl.surepay"))
                .build().apiInfo(prepareApiInfo());
    }

    private ApiInfo prepareApiInfo() {
        return new ApiInfo("Surepay api documents", "This API is designed and provided for Surepay", "1.0", null,
                new Contact("Surepay", "www.surepay.nl", "info@surepay.nl"),
                "surepay lisence", "www.surepay.nl", Collections.emptyList());
    }

}