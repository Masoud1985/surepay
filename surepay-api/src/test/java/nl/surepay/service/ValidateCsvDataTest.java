package nl.surepay.service;

import nl.surepay.commmon.ServiceResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static nl.surepay.enums.TransactionError.DUPLICATION;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ValidateCsvDataTest {

    @InjectMocks
    ValidateCsvData validateCsvData;

    @TempDir
    Path tempDir;

    String transactions =
            "Reference,AccountNumber,Description,Start Balance,Mutation,End Balance\n" +
                    "194261,NL91RABO0315273637,Book John Smith,21.6,-41.83,-20.23\n" +
                    "112806,NL27SNSB0917829871,Clothes Irma Steven,91.23,+15.57,106.8\n" +
                    "183049,NL69ABNA0433647324,Book Arndt Thilo,86.66,+44.5,131.16\n" +
                    "183356,NL74ABNA0248990274,Toy Jimmie Clarice,92.98,-46.65,46.33\n" +
                    "112806,NL69ABNA0433647324,Book Peter de Vries,90.83,-10.91,79.92\n" +
                    "112806,NL93ABNA0585619023,Book Richard Tyson,102.12,+45.87,147.99\n" +
                    "139524,NL43AEGO0773393871,Flowers Jan Tyson,99.44,+41.23,140.67\n" +
                    "179430,NL93ABNA0585619023,Clothes Dolores Kerensa,23.96,-27.43,-3.47\n" +
                    "141223,NL93ABNA0585619023,Book Mahala Kreszenz,94.25,+41.6,135.85\n" +
                    "195446,NL74ABNA0248990274,Toy Hal Shavonne,26.32,+48.98,75.3\n";

    @Test
    public void testPrivateMethodUsingPowerMock() throws Exception {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.csv"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        Map map = Whitebox.invokeMethod(validateCsvData, "prepareTransactionFileData", tempFile.toFile());
        assertThat(map.size() > 0);
//        System.gc();
    }

    @Test
    public void testValidateDataException() throws Exception {
        Map map = Whitebox.invokeMethod(validateCsvData, "prepareTransactionFileData", null);
        assertThat(map).isNull();
    }

    @Test
    public void testValidateData() throws Exception {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.csv"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        ServiceResult serviceResult = validateCsvData.validateData(tempFile.toFile());
        assertThat(serviceResult.isValid());
    }

    @Test
    public void parseTransactions() throws Exception {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.csv"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        Map prepareTransactionFileDataMap = Whitebox.invokeMethod(validateCsvData, "prepareTransactionFileData", tempFile.toFile());
        Map errorList = validateCsvData.parseTransactions(prepareTransactionFileDataMap);
        assertThat(errorList).extractingByKey(DUPLICATION).asList().size().isEqualTo(3);
    }

    @AfterEach
    public void clearGarbage() {
        System.gc();
    }
}