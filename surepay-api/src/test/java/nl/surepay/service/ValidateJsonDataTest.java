package nl.surepay.service;

import nl.surepay.commmon.ServiceResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

import static nl.surepay.enums.TransactionError.INVALID_TRANSACTION;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ValidateJsonDataTest {

    @InjectMocks
    ValidateJsonData validateJsonData;

    @TempDir
    Path tempDir;

    String transactions =
            "[\n" +
                    "   {\n" +
                    "      \"reference\": \"130498\",\n" +
                    "      \"accountNumber\": \"NL69ABNA0433647324\",\n" +
                    "      \"description\": \"Book Jan Theuß\",\n" +
                    "      \"startBalance\": 26.9,\n" +
                    "      \"mutation\": -18.78,\n" +
                    "      \"endBalance\": 8.12\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"167875\",\n" +
                    "      \"accountNumber\": \"NL93ABNA0585619023\",\n" +
                    "      \"description\": \"Toy Greg Alysha\",\n" +
                    "      \"startBalance\": 5429,\n" +
                    "      \"mutation\": -939,\n" +
                    "      \"endBalance\": 6368\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"147674\",\n" +
                    "      \"accountNumber\": \"NL93ABNA0585619023\",\n" +
                    "      \"description\": \"Flowers Julianne Othmar\",\n" +
                    "      \"startBalance\": 74.69,\n" +
                    "      \"mutation\": -44.91,\n" +
                    "      \"endBalance\": 29.78\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"135607\",\n" +
                    "      \"accountNumber\": \"NL27SNSB0917829871\",\n" +
                    "      \"description\": \"Flowers Jan Theuß\",\n" +
                    "      \"startBalance\": 70.42,\n" +
                    "      \"mutation\": 34.42,\n" +
                    "      \"endBalance\": 104.84\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"169639\",\n" +
                    "      \"accountNumber\": \"NL43AEGO0773393871\",\n" +
                    "      \"description\": \"Book Wade Kathie\",\n" +
                    "      \"startBalance\": 31.78,\n" +
                    "      \"mutation\": -6.14,\n" +
                    "      \"endBalance\": 25.64\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"105549\",\n" +
                    "      \"accountNumber\": \"NL43AEGO0773393871\",\n" +
                    "      \"description\": \"Clothes Julianne Othmar\",\n" +
                    "      \"startBalance\": 105.75,\n" +
                    "      \"mutation\": -26.17,\n" +
                    "      \"endBalance\": 79.58\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"150438\",\n" +
                    "      \"accountNumber\": \"NL74ABNA0248990274\",\n" +
                    "      \"description\": \"Toy Jan de Vries\",\n" +
                    "      \"startBalance\": 10.1,\n" +
                    "      \"mutation\": -0.3,\n" +
                    "      \"endBalance\": 9.8\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"172833\",\n" +
                    "      \"accountNumber\": \"NL69ABNA0433647324\",\n" +
                    "      \"description\": \"Book Dirk-Jan Theuß\",\n" +
                    "      \"startBalance\": 66.72,\n" +
                    "      \"mutation\": -41.74,\n" +
                    "      \"endBalance\": 24.98\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"165102\",\n" +
                    "      \"accountNumber\": \"NL93ABNA0585619023\",\n" +
                    "      \"description\": \"Book Shevaun Taylor\",\n" +
                    "      \"startBalance\": 3980,\n" +
                    "      \"mutation\": 1000,\n" +
                    "      \"endBalance\": 4981\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"reference\": \"170148\",\n" +
                    "      \"accountNumber\": \"NL43AEGO0773393871\",\n" +
                    "      \"description\": \"Flowers Morley Koos\",\n" +
                    "      \"startBalance\": 16.52,\n" +
                    "      \"mutation\": 43.09,\n" +
                    "      \"endBalance\": 59.61\n" +
                    "   }\n" +
                    "]";

    @Test
    public void testPrivateMethodUsingPowerMock() throws Exception {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.json"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        Map map = Whitebox.invokeMethod(validateJsonData, "prepareTransactionFileData", tempFile.toFile());
        assertThat(map.size() > 0);
//        System.gc();
    }

    @Test
    public void testValidateDataException() throws Exception {
        Map map = Whitebox.invokeMethod(validateJsonData, "prepareTransactionFileData", null);
        assertThat(map).isNull();
    }

    @Test
    public void testValidateData() throws Exception {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.json"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        ServiceResult serviceResult = validateJsonData.validateData(tempFile.toFile());
        assertThat(serviceResult.isValid());
    }

    @Test
    public void parseTransactions() throws Exception {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.json"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        Map prepareTransactionFileDataMap = Whitebox.invokeMethod(validateJsonData, "prepareTransactionFileData", tempFile.toFile());
        Map errorList = validateJsonData.parseTransactions(prepareTransactionFileDataMap);
        assertThat(errorList).extractingByKey(INVALID_TRANSACTION).asList().size().isEqualTo(2);
    }

    @AfterEach
    public void clearGarbage() {
        System.gc();
    }
}