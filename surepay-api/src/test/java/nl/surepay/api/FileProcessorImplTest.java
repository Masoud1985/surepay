package nl.surepay.api;

import com.fasterxml.jackson.databind.json.JsonMapper;
import nl.surepay.commmon.ApiResponse;
import okhttp3.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.http.HttpStatus;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class FileProcessorImplTest {

    private final String VALIDATE_FILE_URL = "http://localhost:8080/process-file/validateUploadedFile";
    @TempDir
    Path tempDir;
    String transactions =
            "Reference,AccountNumber,Description,Start Balance,Mutation,End Balance\n" +
                    "194261,NL91RABO0315273637,Book John Smith,21.6,-41.83,-20.23\n" +
                    "112806,NL27SNSB0917829871,Clothes Irma Steven,91.23,+15.57,106.8\n" +
                    "183049,NL69ABNA0433647324,Book Arndt Thilo,86.66,+44.5,131.16\n" +
                    "183356,NL74ABNA0248990274,Toy Jimmie Clarice,92.98,-46.65,46.33\n" +
                    "112806,NL69ABNA0433647324,Book Peter de Vries,90.83,-10.91,79.92\n" +
                    "112806,NL93ABNA0585619023,Book Richard Tyson,102.12,+45.87,147.99\n" +
                    "139524,NL43AEGO0773393871,Flowers Jan Tyson,99.44,+41.23,140.67\n" +
                    "179430,NL93ABNA0585619023,Clothes Dolores Kerensa,23.96,-27.43,-3.47\n" +
                    "141223,NL93ABNA0585619023,Book Mahala Kreszenz,94.25,+41.6,135.85\n" +
                    "195446,NL74ABNA0248990274,Toy Hal Shavonne,26.32,+48.98,75.3\n";

    @Test
    void processUploadedFile() throws IOException {
        final Path tempFile = Files.createFile(tempDir.resolve("surepay-transaction.csv"));
        FileWriter fileWriter = new FileWriter(tempFile.toFile());
        fileWriter.write(transactions);
        fileWriter.close();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", tempFile.toFile().getName(),
                        RequestBody.create(tempFile.toFile(), MediaType.parse("application/octet-stream")))
                .build();
        Request request = new Request.Builder()
                .url(VALIDATE_FILE_URL)
                .post(requestBody)
                .build();
        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        Response response = null;
        ApiResponse apiResponse = null;
        response = call.execute();
        assertThat(response.code()).isEqualTo(HttpStatus.OK.value());
        JsonMapper jsonMapper = new JsonMapper();
        apiResponse = jsonMapper.readValue(response.body().byteStream(), ApiResponse.class);
        assertThat(apiResponse.getSuccess()).isTrue();
        assertThat((((List) ((Map) apiResponse.getData()).get("DUPLICATION"))).size()).isEqualTo(3);
    }
}